'use strict';
// process.env.BROWSERIFYSHIM_DIAGNOSTICS = 1;
var gulp = require('gulp');
var runSequence = require('run-sequence');
var path = require('path');
var _ = require('lodash');
var shim = require('browserify-shim');
var nodeResolve = require('resolve');
var merge = require('merge-stream');
var plugins = require('gulp-load-plugins')({
  rename: {
    'gulp-minify-css': 'minifyCSS',
    'gulp-include-source': 'includeSources',
    'gulp-jscs-stylish': 'jscsStylish'
  }
});
var del = require('del');

var watchify = require('watchify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');

var minimist = require('minimist');
var knownOptions = {
  string: 'env',
  default: {
    env: 'develop'
  }
};
var options = minimist(process.argv.slice(2), knownOptions);

var publicFolder = './public/';
var publicJsFolder = publicFolder + 'js/';
var publicCssFolder = publicFolder + 'css/';
var srcFolder = './src/';
var srcJsFolder = srcFolder + 'js/';
var srcLessFolder = srcFolder + 'less/';


var production = (options.env === 'production');
var develop = (options.env === 'develop');

function buildName(prefix) {
  prefix = prefix || 'build';
  return (options.env === 'production') ? prefix + '_' + (new Date()).getTime() : prefix;
}


gulp.task('clean:js', function(cb) {
  del([publicJsFolder + '**/*.*'], cb);
});

gulp.task('clean:css', function(cb) {
  del([publicCssFolder + '**/*.*'], cb);
});


function getNPMPackageIds() {
  return ['jquery', 'backbone', 'backbone.marionette', 'moment'];
}

function getShimmedPackages() {
  var packageManifest = {};
  try {
    packageManifest = require('./package.json');
  } catch (e) {}
  return packageManifest.browser || {};
}

function getShimmedPackageIDs() {
  return _.keys(getShimmedPackages()) || [];
}

function getVendorBundle() {
  var vendorBundle = browserify();


  getNPMPackageIds().forEach(function(id) {
    vendorBundle.require(nodeResolve.sync(id), {
      expose: id
    });
  });

  _.forOwn(getShimmedPackages(), function(path, name) {
    vendorBundle.require(path, {
      expose: name
    });
  });

  vendorBundle.transform('browserify-shim');

  return vendorBundle;
}

function getAppBundle(watch) {
  var appBundle = browserify(srcJsFolder + 'main.js', {
    debug: true,
    paths: ['./src']
  });

  getNPMPackageIds().forEach(function(id) {
    appBundle.external(id);
  });
  getShimmedPackageIDs().forEach(function(id) {
    appBundle.external(id);
  });

  // appBundle.transform(babel);
  appBundle.transform(require('jadeify'));

  return (watch ? watchify(appBundle) : appBundle);

}

function compile(watch) {
  var vendorBundle = getVendorBundle();
  var appBundle = getAppBundle(watch);

  function rebundle() {
    var app = appBundle.bundle()
      .on('error', function(err) {
        console.error(err);
        this.emit('end');
      })
      .pipe(source(buildName('app') + '.js'))
      .pipe(buffer())
      .pipe(plugins.sourcemaps.init({
        loadMaps: true
      }))
      .pipe(plugins.sourcemaps.write('./'))
      .pipe(gulp.dest(publicJsFolder))
      .pipe(plugins.if(develop, plugins.livereload()));


    var vendor = vendorBundle.bundle()
      .pipe(source(buildName('_vendor') + '.js'))
      .pipe(gulp.dest(publicJsFolder));


    return merge(app, vendor);
  }

  if (watch) {
    appBundle.on('update', function() {
      console.log('-> bundling js...');
      del([publicJsFolder + '**/*.*'], rebundle);
    });
  }

  return rebundle();
}


gulp.task('js', ['clean:js'], function() {
  return compile();
});

gulp.task('js:watch', ['clean:js'], function(cb) {
  return compile(true);
});

var reviewedFiles = [srcJsFolder + '**/*.js', 'models/**/*.js', 'config/**/*.js', 'auth/**/*.js', 'app.js'];
gulp.task('js:code-review', function() {
  return gulp.src(reviewedFiles)
    .pipe(plugins.jshint())
    .pipe(plugins.jscs())
    .on('error', function() {})
    .pipe(plugins.jscsStylish.combineWithHintResults())
    .pipe(plugins.jshint.reporter('jshint-stylish'));
});



gulp.task('css', ['clean:css'], function() {
  return gulp.src(srcLessFolder + 'global.less')
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.less())
    .pipe(plugins.autoprefixer({
      browsers: ['> 1%'],
      cascade: true
    }))
    .pipe(plugins.if(production, plugins.minifyCSS()))
    .pipe(plugins.rename(buildName('global') + '.css'))
    .pipe(plugins.sourcemaps.write('./'))
    .pipe(gulp.dest(publicCssFolder))
    .pipe(plugins.if(develop, plugins.livereload()));
});


gulp.task('html', function() {
  var target = gulp.src('./auth/views/layout.jade');
  var sources = gulp.src(['./css/**/*.css', './js/**/*.js'], {
    read: false,
    cwd: 'public'
  });

  return target.pipe(plugins.inject(sources))
    .pipe(gulp.dest('./views'));
});

gulp.task('build', function(callback) {
  runSequence(['js', 'js:code-review', 'css'], 'html', callback);
});


gulp.task('default', ['build']);


gulp.task('dev', ['build'], function(cb) {
  plugins.livereload.listen();

  gulp.start('js:watch');

  plugins.watch([srcLessFolder + '**/*.less'], function() {
    gulp.start('css');
  });


  plugins.watch([publicJsFolder + '**/*.js', publicCssFolder + '**/*.*'], function() {
    gulp.start('html');
  });

  plugins.watch(reviewedFiles, function() {
    gulp.start('js:code-review');
  });

});
