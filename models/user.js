'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// var friends = require('mongoose-friends');
var validate = require('mongoose-validator');
var bcrypt = require('bcrypt-nodejs');
var validator = require('validator');
var async = require('async');
var _ = require('lodash');

//валидаторы имени пользователя
var nameValidator = [
  validate({
    validator: 'isLength',
    arguments: [2, 32],
    message: 'Name should be between 2 and 32 characters'
  }),
  // validate({
  //   validator: 'isAlpha',
  //   passIfEmpty: true,
  //   message: 'Name should contain only letters'
  // }),
  validate({
    validator: 'matches',
    arguments: /^[a-zA-Za-яёА-ЯЁ\s]*$/,
    message: 'Name should contain only letters'
  })
];

//валидаторы логина пользователя
var loginValidator = [
  validate({
    validator: function(val) {
      return !!val;
    },

    message: 'Login field is required'
  }),
  validate({
    validator: 'isLength',
    arguments: [4, 8],
    message: 'Login should be between 4 and 8 characters'
  }),
  validate({
    validator: 'isAlphanumeric',
    message: 'Login should contain alpha-numeric characters only'
  }),
  validate({
    validator: function(val) {
      return isNaN(parseInt(val[0], 10));
    },

    message: 'Login could not start with a digit'
  })
];

//валидаторы пароля пользователя
var passwordValidator = [
  validate({
    validator: function(val) {
      return !!val;
    },

    message: 'Password field is required'
  })
];

var FriendshipShema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  status: {
    type: String,
    required: true,
    enum: ['follower', 'friend', 'following']
  }
});

var UserSchema = new Schema({
  name: {
    type: String,
    validate: nameValidator
  },
  login: {
    type: String,
    required: true,
    unique: true,
    validate: loginValidator
  },
  password: {
    type: String,
    required: true,
    validate: passwordValidator,
    set: function(password) {
      return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
    }
  },
  friends: [FriendshipShema],
  createdAt: {
    type: Date
  },
  updatedAt: {
    type: Date
  }
});

UserSchema.pre('save', function(next) {
  var now = new Date();
  this.updatedAt = now;
  if (!this.createdAt) {
    this.createdAt = now;
  }

  next();
});

// UserSchema.plugin(friends());

//дополнительные методы модели

//метод для сравнение пароля и его хеша
UserSchema.methods.checkPassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};

UserSchema.methods.getFriendship = function(userId) {
  var friendship;

  if (this.friends) {
    friendship = _.find(this.friends, function(_friendship) {
      return _friendship.user == userId;
    });
  }

  return friendship;
};

UserSchema.methods.getFriendshipStatus = function(userId) {
  var friendship = this.getFriendship(userId);
  var status;

  if (friendship) {
    status = friendship.status;
  }

  return status;
};

//подать заявку на дружбу от пользователя к пользователю2
//userId - id пользователя2
//callback - стандартный callback
UserSchema.methods.follow = function(userId, callback) {
  var user1 = this;
  var user1Id = this.id;
  var user2Id = userId;

  async.waterfall([

    //находим пользователя2 которому подаём заявку на дружбу
    function(callback) {
      User.findById(user2Id, callback);
    },

    function(user2, callback) {
      var status = user1.getFriendshipStatus(user2Id);
      console.log('status', status);

      async.parallel([
          function(callback) {
            if (!status) { //если это первоначальная заявка, то ставим себе статус дружбы following, у него статус дружбы follower
              user1.friends.push({
                user: user2Id,
                status: 'following'
              });
              user1.save(callback);
            } else if (status === 'follower') { //если пользователь2 уже имеет статус дружбы following, то ставив у себя и у него статус дружбы frend
              var friendship = user1.getFriendship(user2Id);
              friendship.status = 'friend';
              user1.save(callback);
            } else {
              callback();
            }
          },

          function(callback) {

            if (!status) {
              user2.friends.push({
                user: user1Id,
                status: 'follower'
              });
              user2.save(callback);
            } else if (status === 'follower') {
              var friendship = user2.getFriendship(user1Id);
              friendship.status = 'friend';
              user2.save(callback);
            } else {
              callback();
            }
          }

        ],
        callback);
    }

  ], function(err) {
    if (err) {
      callback(err);
    } else {
      callback(null, {
        status: 'ok'
      });
    }
  });

};

UserSchema.methods.getFriendshipsWithStatus = function(status, callback) {
  if (this.friends) {
    var ids = _.chain(this.friends).filter(function(frendship) {
      return frendship.status === status;
    }).map(function(frendship) {
      return frendship.user;
    }).value();

    User.find()
      .where('_id').in(ids)
      .select('name login')
      .exec(callback);

  } else {
    callback(null, []);
  }
};

UserSchema.methods.getAllFriends = function(callback) {
  if (this.friends) {
    var ids = _.map(this.friends, function(frendship) {
      return frendship.user;
    });

    User.find()
      .where('_id').in(ids)
      .select('name login friends')
      .exec(callback);

  } else {
    callback(null, []);
  }
};

UserSchema.methods.getFriends = function(callback) {
  this.getFriendshipsWithStatus('friend', callback);
};

UserSchema.methods.getFollowers = function(callback) {
  this.getFriendshipsWithStatus('follower', callback);
};

UserSchema.methods.getFollowing = function(callback) {
  this.getFriendshipsWithStatus('following', callback);
};

//отменить заявку в друзья от пользователя1 к пользователю2
//userId - id пользователя2
//callback - стандартный callback
UserSchema.methods.unfollow = function(userId, callback) {
  var user1 = this;
  var user1Id = this.id;
  var user2Id = userId;

  async.waterfall([

    //находим пользователя2 которому отменяем заявку на дружбу
    function(callback) {
      User.findById(user2Id, callback);
    },

    function(user2, callback) {
      var status = user1.getFriendshipStatus(user2Id);
      console.log('status', status);

      async.parallel([
          function(callback) {
            var friendship = user1.getFriendship(user2Id);

            if (status === 'friend') { //если сейчас у пользователей статус дружбы frend, то ставим у себя статус дружбы follower, у пользователя2 статус дружбы following
              friendship.status = 'follower';
              user1.save(callback);
            } else if (status === 'following') { //если сейчас пользователь1 только подал заявку на дружбу, то удаляем дружбу
              friendship.remove();
              user1.save(callback);
            } else {
              callback();
            }

          },

          function(callback) {
            var friendship = user2.getFriendship(user1Id);

            if (status === 'friend') {
              friendship.status = 'following';
              user2.save(callback);
            } else if (status === 'following') {
              friendship.remove();
              user2.save(callback);
            } else {
              callback();
            }

          }

        ],
        callback);

    }

  ], function(err) {
    if (err) {
      callback(err);
    } else {
      callback(null, {
        status: 'ok'
      });
    }
  });

};

//валидация пароля
UserSchema.statics.validatePassword = function(password) {
  var errors = [];

  if (!validator.isLength(password, 6, 12)) {
    errors.push(new Error('Password should be between 6 and 12 characters'));
  }

  if (!validator.matches(password, /[a-zA-Z]/)) {
    errors.push(new Error('Password should contain at least one letter'));
  }

  if (!validator.matches(password, /\d/)) {
    errors.push(new Error('Password should contain at least one number'));
  }

  return errors;
};

var User = mongoose.model('User', UserSchema);

module.exports = User;
