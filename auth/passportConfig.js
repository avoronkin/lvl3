'use strict';
var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');
var ware = require('ware');

module.exports = function(passport) {
  passport.serializeUser(function(user, done) {
    done(null, user._id);
  });

  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });

  passport.use('lvl3', new LocalStrategy({
      usernameField: 'login',
      passwordField: 'password',
      passReqToCallback: true
    },
    function(req, login, password, done) {
      var name = req.body.name;

      var middleware = ware()
        .use(function(params, next) {
          // ищем пользователя с таким логином
          User.findOne({
            login: login
          }, function(err, _user) {
            if (err) {
              next(err);
            } else {
              params.user = _user;
              next();
            }
          });

        })
        .use(function(params, next) {
          //если есть пользователь, то проверяем пароль
          var user = params.user;

          if (user && !user.checkPassword(password)) {
            next(new Error('Wrong password'));
          } else {
            next();
          }

        }).use(function(params, next) {
          //если есть пользователь и задано новое имя, то меняем имя
          var user = params.user;

          if (user && name) {
            user.name = name;
            user.save(next);
          } else {
            next();
          }

        }).use(function(params, next) {
          //если пользователя нет, то создаём его
          if (!params.user) {
            //проверяем валидность пароля
            var errors = User.validatePassword(password);
            if (errors.length) {
              next(errors);
            }

            params.user = new User({
              name: name,
              login: login,
              password: password
            });

            params.user.save(next);
          } else {
            next();
          }

        });

      middleware.run({}, function(err, params) {

        if (err) {
          done(err);
        } else {
          done(null, params.user);
        }

      });

    }));
};
