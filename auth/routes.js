'use strict';

var express = require('express');
var passport = require('passport');
var router = express.Router();
var moment = require('moment');
var isAuthenticated = require('./isAuthenticated');
var _ = require('lodash');

//главная страница
router.get('/', function(req, res) {
  var responce = {};
  responce.errors = req.flash('error');

  if (req.user) {
    responce.user = {
      id: req.user.id,
      name: req.user.name,
      login: req.user.login,
      registered: moment(req.user.registered).format('DD/MM/YYYY')
    };
  }

  res.render('index', responce);
});

function getErrorMessages(errors) {
  var messages = [];
  if (_.isObject(errors) && !_.isArray(errors)) {
    if (errors.errors) {
      Object.keys(errors.errors).forEach(function(key) {
        messages.push(errors.errors[key].message);
      });

    } else {
      console.log('error', errors.type);
      messages.push(errors.message);
    }
  }

  if (_.isArray(errors)) {
    errors.forEach(function(error) {
      messages.push(error.message);
      console.log('err', error, messages);
    });
  }

  return messages;
}

//обработка формы авторизации
router.post('/login', function(req, res, next) {
  function callback(error) {
    if (error) {
      req.flash('error', getErrorMessages(error));
    }

    res.redirect('/');
  }

  passport.authenticate('lvl3', function(err, user, info) {
    if (err) {
      return callback(err);
    }

    if (!user) {
      return callback(new Error((info && info.message) ? info.message : 'User not found'));
    } else {
      req.logIn(user, callback);
    }

  })(req, res, next);

});

router.get('/logout', function(req, res) {
  req.logout();
  res.redirect('/');
});

// router.get('/*', function(req, res) {
//   res.render('index');
// });

module.exports = router;
