'use strict';

var express = require('express');
var path = require('path');
var passport = require('passport');
var flash = require('connect-flash');
var routes = require('./routes');

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

app.use('/', routes);

// passport config
require('./passportConfig')(passport);

module.exports = app;
