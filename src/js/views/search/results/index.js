'use strict';

var ListOfUsers = require('../../list-of-users');
var EmptyView = require('./empty');

module.exports = ListOfUsers.extend({
  emptyView: EmptyView,
});
