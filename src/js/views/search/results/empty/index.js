'use strict';

var Marionette = require('backbone.marionette');
var template = require('./template.jade');

module.exports = Marionette.ItemView.extend({
  template: template,
  tagName: 'ul'
});
