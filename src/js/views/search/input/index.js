'use strict';

var Marionette = require('backbone.marionette');
var template = require('./template.jade');
var $ = require('jquery');

var delay = (function() {
  var timer = 0;
  return function(callback, ms) {
    clearTimeout(timer);
    timer = setTimeout(callback, ms);
  };
})();

module.exports = Marionette.ItemView.extend({
  initialize: function() {},

  template: template,
  className: 'textbox icon',
  events: {
    'keyup input': 'onChange'
  },
  onChange: function(e) {
    var _this = this;
    var $target = $(e.target);

    delay(function() {
      var text = $target.val();
      _this.collection.filter.set('search', text);
    }, 200);

  }
});
