'use strict';

var Marionette = require('backbone.marionette');
var template = require('./template.jade');
var _ = require('lodash');
var $ = require('jquery');

module.exports = Marionette.ItemView.extend({
  initialize: function() {
    this.listenTo(this.collection, 'add remove reset', this.render);
    this.listenTo(this.collection.filter, 'change', this.render);
  },
  template: template,
  events: {
    'click a': 'onClick'
  },
  onClick: function(e) {
    e.preventDefault();
    e.stopPropagation();
    var $target = $(e.target);
    this.collection.filter.set('page', $target.text().trim());
  },
  serializeData: function() {
    var data = {};
    data.total = this.collection.count;
    data.limit = this.collection.filter.get('limit');

    var pagesNum = Math.ceil(data.total / data.limit);
    data.page = this.collection.filter.get('page');
    data.pages = _.range(1, pagesNum + 1, 1);

    console.log('pager data', data, pagesNum, this);
    return data;
  }
});
