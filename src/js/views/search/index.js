'use strict';

var Marionette = require('backbone.marionette');
var template = require('./template.jade');
var InputView = require('./input');
var ResultsView = require('./results');
var PagerView = require('./pager');

module.exports = Marionette.LayoutView.extend({
  template: template,
  regions: {
    'input': '.search-input',
    'results': '.search-results',
    'pager': '.search-pager'
  },
  onBeforeShow: function() {
    this.showChildView('input', new InputView({
      collection: this.collection
    }));

    this.showChildView('results', new ResultsView({
      collection: this.collection,
      model: this.model
    }));

    this.showChildView('pager', new PagerView({
      collection: this.collection,
      model: this.model
    }));
  }
});
