'use strict';

var Marionette = require('backbone.marionette');
var template = require('./template.jade');
var $ = require('jquery');

module.exports = Marionette.ItemView.extend({
  template: template,
  className: 'navigation',
  events: {
    'click a': 'onClick'
  },
  onClick: function(e) {
    $(e.target).parents('li')
      .addClass('active')
      .siblings().removeClass('active');
  }
});
