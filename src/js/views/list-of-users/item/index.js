'use strict';

var Marionette = require('backbone.marionette');
var template = require('./template.jade');

module.exports = Marionette.ItemView.extend({
  template: template,
  tagName: 'li',
  initialize: function(options) {
    this.user = options.user;
    this.listenTo(this.model, 'change', this.render);
  },

  events: {
    'click .follow': 'follow',
    'click .unfollow': 'unfollow'
  },

  follow: function() {
    this.user.follow(this.model);
  },

  unfollow: function() {
    this.user.unfollow(this.model);
  },

  serializeData: function() {
    var data = this.model.toJSON();
    data.status = this.user.getStatusFor(this.model.id);
    data.userId = this.user.id;

    return data;
  }
});
