'use strict';

var Marionette = require('backbone.marionette');
var ItemView = require('./item');

module.exports = Marionette.CollectionView.extend({
  initialize: function() {
    this.listenTo(this.collection, 'change', this.render);
    this.listenTo(this.model, 'change', this.render);
  },
  childView: ItemView,

  tagName: 'ul',
  childViewOptions: function() {
    return {
      user: this.model
    };
  }

});
