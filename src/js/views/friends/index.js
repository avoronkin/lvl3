'use strict';

var Marionette = require('backbone.marionette');
var template = require('./template.jade');
var FriendsView = require('./friends');
var FollowersView = require('./followers');
var FollowingView = require('./following');

module.exports = Marionette.LayoutView.extend({
  template: template,
  regions: {
    friends: '.friends',
    followers: '.followers',
    following: '.following'
  },
  onBeforeShow: function() {
    this.showChildView('friends', new FriendsView({
      collection: this.collection,
      model: this.model
    }));

    this.showChildView('followers', new FollowersView({
      collection: this.collection,
      model: this.model
    }));

    this.showChildView('following', new FollowingView({
      collection: this.collection,
      model: this.model
    }));
  }
});
