'use strict';

var ListOfUsers = require('../../list-of-users');

module.exports = ListOfUsers.extend({
  filter: function(model) {
    var status = this.model.getStatusFor(model.id);
    return status === 'following';
  }
});
