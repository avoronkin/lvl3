'use strict';

var Marionette = require('backbone.marionette');
var template = require('./template.jade');
var moment = require('moment');

module.exports = Marionette.ItemView.extend({
  initialize: function() {
    this.listenTo(this.model, 'change', this.render);
  },

  template: template,
  serializeData: function() {
    var data = this.model.toJSON();
    data.createdAt = moment(this.model.get('createdAt')).format('DD/MM/YYYY');

    return data;
  }
});
