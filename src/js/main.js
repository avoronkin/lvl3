'use strict';

var Backbone = require('backbone');
var $ = require('jquery');
Backbone.$ = $;
var Marionette = require('backbone.marionette');

var rm = new Marionette.RegionManager();
rm.addRegions({
  main: '#main-content',
  navigation: '#navigation'
});

var User = require('./models/user');
var Users = require('./models/users');
var Search = require('./models/search');
var Friends = require('./models/friends');
var NavigationView = require('./views/navigation');
var ProfileView = require('./views/profile');
var FriendsView = require('./views/friends');
var SearchView = require('./views/search');

rm.get('navigation').show(new NavigationView());

var Router = Marionette.AppRouter.extend({
  initialize: function() {
    $('body').on('click', 'a:not(a[data-bypass])', function(e) {
      e.preventDefault();
      var href = $(this).attr('href');
      Backbone.history.navigate(href, true);
    });
  },

  routes: {
    '': 'profile',
    'user/:id': 'profile',
    friends: 'friends',
    search: 'search'
  },

  profile: function(id) {
    console.log('profile', arguments);
    var user = new User({
      _id: id || userId
    });
    user.fetch();
    rm.get('main').show(new ProfileView({
      model: user
    }));
  },

  friends: function() {
    var user = new User({
      _id: userId
    });
    user.fetch();

    var friends = new Friends({
      user: user
    });

    friends.fetch();

    rm.get('main').show(new FriendsView({
      collection: friends,
      model: user
    }));

    console.log('friends');
  },

  search: function() {
    var search = new Search();
    search.search();

    var user = new User({
      _id: userId
    });
    user.fetch();

    rm.get('main').show(new SearchView({
      collection: search,
      model: user
    }));
    console.log('search');
  }

});

$(document).ready(function() {
  new Router();
  Backbone.history.start({
    pushState: true
  });
});
