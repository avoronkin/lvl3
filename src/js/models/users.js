'use strict';

var Backbone = require('backbone');
var User = require('./user');

module.exports = Backbone.Collection.extend({
  url: '/api/v1/users',
  model: User
});
