'use strict';

var Users = require('./users');
var Backbone = require('backbone');
var Filter = Backbone.Model.extend({});

module.exports = Users.extend({
  initialize: function() {
    this.filter = new Filter({
      limit: 5,
      page: 1,
      search: ''
    });

    this.listenTo(this.filter, 'change:search', function() {
      this.filter.set('page', 1);
    });

    this.listenTo(this.filter, 'change', this.search);
  },
  url: '/api/v1/users/search',
  search: function() {
    this.fetch({
      data: this.filter.toJSON()
    });
  },

  parse: function(response) {
    this.count = response.count;
    return response.users;
  }
});
