'use strict';

var Users = require('./users');

module.exports = Users.extend({
  initialize: function(options) {
    this.user = options.user;
  },

  url: function() {
    return '/api/v1/users/' + this.user.id + '/allfriends';
  }
});
