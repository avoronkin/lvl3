'use strict';

var Backbone = require('backbone');
var _ = require('lodash');
var $ = require('jquery');

module.exports = Backbone.Model.extend({
  urlRoot: '/api/v1/users',
  idAttribute: '_id',
  defaults: {
    name: '',
    login: '',
    createdAt: ''
  },

  follow: function(user) {
    var url = '/api/v1/users/' + this.id + '/follow';
    var _this = this;

    $.post(url, {
      id: user.id
    }, function(data) {
      if (data.status === 'ok') {
        _this.fetch();
        user.fetch();
      }
    });

  },

  unfollow: function(user) {
    var url = '/api/v1/users/' + this.id + '/unfollow';
    var _this = this;

    $.post(url, {
      id: user.id
    }, function(data) {
      if (data.status === 'ok') {
        _this.fetch();
        user.fetch();
      }
    });
  },

  getStatusFor: function(userId) {
    var status;
    var friends = this.get('friends') || [];

    var friendship = _.find(friends, function(_friendship) {
      return _friendship.user === userId;
    });

    if (friendship) {
      status = friendship.status;
    }

    return status;
  }
});
