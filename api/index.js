'use strict';

var express = require('express');
var methodOverride = require('method-override');
var restify = require('express-restify-mongoose');
var User = require('../models/user');
var async = require('async');

var app = express();
var router = express.Router();

app.use(methodOverride());

//запросы к апи только для авторизованных пользователей
// app.use(function(req, res, next) {
//   if (req.session && req.session.passport && req.session.passport.user) {
//     return next();
//   }

//   res.json({
//     error: 'not authenticated!'
//   });
// });

function showResults(req, res, next) {
  return function(err, results) {
    if (err) {
      next(err);
    } else {
      res.json(results);
    }
  };
}

//поиск пользователей по имени
router.get('/v1/users/search', function(req, res, next) {
  var regex = new RegExp(req.query.search, 'i');
  var query = {
    name: regex
  };

  //текущий пользователь не попадает в результаты
  // if (req.session && req.session.passport && req.session.passport.user) {
  //   query._id = {
  //     '$ne': req.session.passport.user
  //   };
  // }

  //паджинация
  var limit = req.query.limit || 10;
  var skip = limit * ((req.query.page || 1) - 1);

  async.parallel({
      users: function(callback) {
        User.find(query)
          .limit(limit)
          .skip(skip)
          .exec(callback);
      },
      count: function(callback) {
        User.count(query).exec(callback);
      }
    },
    showResults(req, res, next));


});


//подписки
router.get('/v1/users/:userId/following', function(req, res, next) {
  var userId = req.params.userId;
  async.waterfall([
      function(callback) {
        User.findById(userId, callback);
      },
      function(user, callback) {
        user.getFollowing(callback);
      }
    ],
    showResults(req, res, next));
});

//подписчики
router.get('/v1/users/:userId/followers', function(req, res, next) {
  var userId = req.params.userId;
  async.waterfall([
      function(callback) {
        User.findById(userId, callback);
      },
      function(user, callback) {
        user.getFollowers(callback);
      }
    ],
    showResults(req, res, next));
});

//друзья
router.get('/v1/users/:userId/friends', function(req, res, next) {
  var userId = req.params.userId;
  async.waterfall([
      function(callback) {
        User.findById(userId, callback);
      },
      function(user, callback) {
        user.getFriends(callback);
      }
    ],
    showResults(req, res, next));
});

router.get('/v1/users/:userId/allfriends', function(req, res, next) {
  var userId = req.params.userId;
  async.waterfall([
      function(callback) {
        User.findById(userId, callback);
      },
      function(user, callback) {
        user.getAllFriends(callback);
      }
    ],
    showResults(req, res, next));
});

//подать заявку на дружбу или подтвердить заявку
router.post('/v1/users/:userId/follow', function(req, res, next) {
  var userId = req.params.userId;
  var frendId = req.body.id;

  async.waterfall([
      function(callback) {
        User.findById(userId, callback);
      },
      function(user, callback) {
        user.follow(frendId, callback);
      }
    ],
    showResults(req, res, next));

});

//отменить "дружбу"
router.post('/v1/users/:userId/unfollow', function(req, res, next) {
  var userId = req.params.userId;
  var frendId = req.body.id;

  async.waterfall([
      function(callback) {
        User.findById(userId, callback);
      },
      function(user, callback) {
        user.unfollow(frendId, callback);
      }
    ],
    showResults(req, res, next));

});

restify.serve(router, User, {
  prefix: '',
  protected: 'password'
});



app.use(router);

module.exports = app;
