'use strict';


var expect = require('chai').expect;
var dbURI = 'mongodb://localhost/lvl3-test';
var mongoose = require('mongoose');
var clearDB = require('mocha-mongoose')(dbURI);
var User = require('../models/user');
var bcrypt = require('bcrypt-nodejs');

describe('User model', function() {
  before(function(done) {
    if (mongoose.connection.db) {
      return done();
    }

    mongoose.connect(dbURI, done);
  });

  beforeEach(function(done) {
    clearDB(done);
  });

  var userParams = {
    login: 'login',
    password: 'password1'
  };

  it('can be created', function(done) {
    var user = new User(userParams);

    user.save(function(err) {
      expect(err).to.not.exist;
      done();
    });
  });

  describe('Name field', function() {
    it('can contain only letters', function(done) {
      var user = new User({
        name: 'name',
        login: 'login',
        password: 'password1',
      });

      user.save(function(err) {
        expect(err).to.not.exist;
        done();
      });
    });

    it('can\' contain numbers', function(done) {
      var user = new User({
        name: 'name2',
        login: 'login',
        password: 'password1',
      });

      user.save(function(err) {
        expect(err).to.exist;
        done();
      });
    });

    it('can\'t contain punctuation', function(done) {
      var user = new User({
        name: 'name!',
        login: 'login',
        password: 'password1',
      });

      user.save(function(err) {
        expect(err).to.exist;
        done();
      });
    });

    it('min length:2', function(done) {
      var user = new User({
        name: 'n',
        login: 'login',
        password: 'password1',
      });

      user.save(function(err) {
        expect(err).to.exist;
        done();
      });
    });

    it('max length:32', function(done) {
      var user = new User({
        name: 'nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn',
        login: 'login',
        password: 'password1',
      });

      user.save(function(err) {
        expect(err).to.exist;
        done();
      });
    });


  });

  describe('Login field', function() {
    it('is required', function(done) {
      var user = new User({
        password: 'password'
      });
      user.save(function(err) {
        expect(err).to.exist;
        done();
      });
    });

    it('min length:4', function(done) {
      var user = new User({
        login: 'log',
        password: 'password1',
      });

      user.save(function(err) {
        expect(err).to.exist;
        done();
      });
    });

    it('max length:8', function(done) {
      var user = new User({
        login: 'loginlogin',
        password: 'password1',
      });

      user.save(function(err) {
        expect(err).to.exist;
        done();
      });
    });

    it('can contain alpha-numeric characters only', function(done) {
      var user = new User({
        login: 'login22',
        password: 'password1',
      });

      user.save(function(err) {
        expect(err).to.not.exist;
        done();
      });
    });

    it('can\'t contain not alpha-numeric characters', function(done) {
      var user = new User({
        login: 'lo@!()2',
        password: 'password1',
      });

      user.save(function(err) {
        expect(err).to.exist;
        done();
      });
    });

  });


  describe('Password field', function(done) {
    it('is required', function(done) {
      var user = new User({
        login: 'login'
      });
      user.save(function(err) {
        expect(err).to.exist;
        done();
      });
    });

    it('should be saved as hash', function(done) {
      var user = new User({
        password: 'password1',
        login: 'login'
      });
      user.save(function(err) {
        expect(user.password).to.be.not.equal('password1');
        expect(user.checkPassword('password1')).to.be.ok;
        done();
      });
    });

    describe('User.validatePassword method', function() {
      it('should return error if password length is less than 6', function() {
        var errors = User.validatePassword('pass1');
        expect(errors).to.be.not.empty;
      });

      it('should return error if password length is more than 12', function() {
        var errors = User.validatePassword('password2passwor2');
        expect(errors).to.be.not.empty;
      });

      it('should return error if password don\'t contain at least one letter', function() {
        var errors = User.validatePassword('12345678');
        expect(errors).to.be.not.empty;
      });

      it('should return error if password don\'t contain at least one number', function() {
        var errors = User.validatePassword('password');
        expect(errors).to.be.not.empty;
      });

      it('should return empty array if password is ok', function() {
        var errors = User.validatePassword('pass1232');
        expect(errors).to.be.empty;

      });
    });



  });

  describe('CreatedAt field', function() {
    it('should be automatically created when the model is created', function(done) {
      var user = new User(userParams);

      user.save(function(err) {
        expect(err).to.not.exist;
        expect(user.createdAt).to.exist;

        done();
      });

    });

  });



});
